import './styles/app.scss';
import 'bootstrap/dist/js/bootstrap.bundle.js';
import 'datatables.net';
import 'datatables.net-bs5';

import './bootstrap.js';
// $('#datatable').DataTable();
$(document).ready(function() {
  $('#datatable').DataTable({
    'language': {
      'lengthMenu': 'Afficher _MENU_ enregistrements par page',
      'zeroRecords': 'Aucun enregistrement trouvez - oops',
      'info': 'Montrer page _PAGE_ de _PAGES_',
      'infoEmpty': 'Pas d\'enregistrement touver',
      'infoFiltered': '(filtrer à apartir de _MAX_ totales enregistrements)',
      'search': 'rechercher',
      'paginate': {
        'first': 'Premier',
        'last': 'Dernier',
        'next': 'Suivant',
        'previous': 'Précédent'
      },
    },

  });
});

// register globally for all charts
document.addEventListener('chartjs:init', function(event) {
  const Chart = event.detail.Chart;
  const Tooltip = Chart.registry.plugins.get('tooltip');
  Tooltip.positioners.bottom = function(items) {
    /* ... */
  };
});
// import './admin2/jquery/jquery.min.js';

// import './admin2/bootstrap/js/bootstrap.bundle.min.js';
// import './admin2/jquery-easing/jquery.easing.min.js';
// import './admin2/js/sb-admin-2.min.js';
// import './admin2/chart.js/Chart.min.js';
// import './admin2/js/demo/chart-area-demo.js';
// import './admin2/js/demo/chart-pie-demo.js';

$('#btnOneclic').on('click', function() {
  $('#start').val();
  $('#datedebuttosubmit').val();
  $('#datedebuttosubmit').val($('#start').val());

  // alert('start =' + $('#start').val() + '/---- datedebuttosubmit =' +
  // $('#datedebuttosubmit').val());
});

$('#btnTwoclic').on('click', function() {
  $('#start2').val();
  $('#end2').val();
  $('#datedebuttosubmit').val();
  $('#datefintosubmit').val();

  $('#datedebuttosubmit').val($('#start2').val());
  $('#datefintosubmit').val($('#end2').val());

  // alert('start =' + $('#start2').val() + '/----- end =' + $('#end2').val() +
  //     '/---- datedebuttosubmit =' + $('#datedebuttosubmit').val() +
  //     '/---datefintosubmit  =' + $('#datefintosubmit').val());
});

// import for table
// import './jquery-3.7.0.js';
// import './js/jquery.dataTables.min.js';
// import './js/dataTables.bootstrap5.min.js'
// import './datatables.min.css'
// import './datatables.min.js'

// import DataTable from 'datatables.net-bs5';
// import './styles/new.css';
// $('#datatable').DataTable();

// const exampleModal = document.getElementById('exampleModal')
// if (exampleModal) {
//   exampleModal.addEventListener('show.bs.modal', event => {
//     // Button that triggered the modal
//     const button = event.relatedTarget
//   // Extract info from data-bs-* attributes
//   const recipient = button.getAttribute('data-bs-whatever')
//   // If necessary, you could initiate an Ajax request here
//   // and then do the updating in a callback.

//   // Update the modal's content.
//   const modalTitle = exampleModal.querySelector('.modal-title')
//   const modalBodyInput = exampleModal.querySelector('.modal-body input')

//   modalTitle.textContent = `New message to ${recipient}`
//     modalBodyInput.value = recipient
//   })
// }
// import './calandar.js';
// import './moment-with-locales.min.js';
// import './bootstrap-datetimepicker.min.js';