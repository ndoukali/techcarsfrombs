<?php

namespace App\Controller;

use ApiPlatform\Api\QueryParameterValidator\Validator\Length;
use App\Entity\TableJournale;
use Symfony\UX\Chartjs\Model\Chart;
use App\Repository\TableJournaleRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

    function date_range($first, $last, $step = '+1 day', $output_format = 'd/m/Y' ) {

    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);

    while( $current <= $last ) {

        $dates[] = date($output_format, $current);
        $current = strtotime($step, $current);
    }

    return $dates;
    }

class StatsController extends AbstractController
{

    #[Route('/stats', name: 'app_stats')]
    public function index(ChartBuilderInterface $chartBuilder, TableJournaleRepository $repo, Request $request): Response
    {
        $machinesnamearrays  = array();
        $machinesnamearray  = array();
        $now = new \DateTime();
        $datedebut = $now->format("Y-m-d");
        $datefin = $now->format("Y-m-d");
        
        $selectdateString = [
                "Ajourd'hui"  ,
                "Hier",
                "Avant hier" ,
                "choisir jour " ,
                "Entre deux dates",
        ];


        if ($request->request->count() > 0) {
            // $datedebut = $request->request->get('startdate');
            // $datefin = $request->request->get('enddate');
            $machine = $request->request->get('machine');
            $selectdate = $request->request->get('selectdate');
            if ($selectdate == '0') {
                $datedebut = $now->format("Y-m-d");
            }elseif ($selectdate == '1') {
                $now = $now->sub(new \DateInterval('P1D'));
                $datedebut = $now->format("Y-m-d");
            }elseif ($selectdate == '2') {
                $now = $now->sub(new \DateInterval('P2D'));
                $datedebut = $now->format("Y-m-d");
            }elseif ($selectdate == '3') {
                $datedebut = $request->request->get('startdate');
            }elseif ($selectdate == '4') {
                $datedebut = $request->request->get('startdate');
                $datefin = $request->request->get('enddate');
            }          
            
        }else {
            $machine = "m15";
            
            $datedebut = $now->format("Y-m-d");
            $datefin = $now->format("Y-m-d");
            $datas = $repo->findAll();
        }
        $daterange = date_range($datedebut, $datefin);
        dump($selectdate);
        dump($datedebut);
        dump($datefin);
        dump($daterange);
        $data = $repo->findBymachineName($machine);
        // $datas = $repo->findAll();
        // $datedebutdatetime = strtotime($datedebut);
        // $datedebutD = $datedebut->format('Y-m-d');
        // $datefinD = $datefin->format('Y-m-d');
        if (count($daterange) > 1) {
          $datas = $repo->getReport($datedebut, $datefin);
        }else {
            
            $datas = $repo->findByCreatedAt(\DateTimeImmutable::createFromFormat('Y-m-d',  $datedebut));
        }

        
        dump($datas);
        $dataForMacineName = $repo->findAll();
        // $unchamp = $datas[1]->getMachineName();
        if (count($dataForMacineName) > 0) {
            // dump("hello");
            foreach ($dataForMacineName as $key => $machinetmp) {
              $machinesnamearray[count($machinesnamearray)] = $dataForMacineName[$key]->getMachineName();
            }
        }
        
        if (count($machinesnamearray) > 0) {
            foreach ($machinesnamearray as $key => $machinesname) {
                $machinetmp = $machinesnamearray[$key];
                if (!(in_array($machinetmp , $machinesnamearrays ))) {
                    if ($machinetmp != null ) {
                        $machinesnamearrays[count($machinesnamearrays)] = $machinetmp;  
                    }
                
                }
            }
        }
        // dump($machinesnamearrays);
        // var_dump($unchamp);
        // nombre de points par heures
        $chart = $chartBuilder->createChart(Chart::TYPE_BAR);
        $chart->setData([
            
            'labels' => ['6H', '7H', '8H', '9H', '10H', '11H', '12H', '13H', '14H', '15H', '16H', '17H', '18H'],
            // $retVal = ($data[1]->getPointnumber()) ? $data[1]->getPointnumber() : 10 ;
            'datasets' => [
                [
                    'label' => $machine,
                    'backgroundColor' => 'rgb(255, 99, 132, .4)',
                    'borderColor' => 'rgb(255, 99, 132)',
                    'data' => [
                        ((count($datas) > 0) ? $datas[0]->getPointnumber() : 0 ),
                        ((count($datas) > 1) ? $datas[1]->getPointnumber() : 0 ),
                        ((count($datas) > 2) ? $datas[2]->getPointnumber() : 0 ),
                        ((count($datas) > 3) ? $datas[3]->getPointnumber() : 0 ),
                        ((count($datas) > 4) ? $datas[4]->getPointnumber() : 0 ),
                        ((count($datas) > 5) ? $datas[5]->getPointnumber() : 0 ),
                        ((count($datas) > 6) ? $datas[6]->getPointnumber() : 0 ),
                        ((count($datas) > 7) ? $datas[7]->getPointnumber() : 0 ),
                        ((count($datas) > 8) ? $datas[8]->getPointnumber() : 0 ),
                        ((count($datas) > 9) ? $datas[9]->getPointnumber() : 0 ),
                        ((count($datas) > 10) ? $datas[10]->getPointnumber() : 0 ),
                        ((count($datas) > 11) ? $datas[11]->getPointnumber() : 0 ),
                        ((count($datas) > 12) ? $datas[12]->getPointnumber() : 0 ),
                                ],
                    'tension' => 0.4,
                ],
                // [
                //     'label' => 'Machine B15',
                //     'backgroundColor' => 'rgba(45, 220, 126, .4)',
                //     'borderColor' => 'rgba(45, 220, 126)',
                //     'data' => [10, 15, 4, 3, 25, 41, 25],
                //     'tension' => 0.4,
                // ],
            ],
        ]);
        $chart->setOptions([
            'maintainAspectRatio' => false,
        ]);
        $camembert = $chartBuilder->createChart(Chart::TYPE_PIE);
        $camembert->setData([
            'labels' => ['Vitesse reduite', 'En Arret', 'En travail'],
            
            'datasets' => [
                [
                    // 'label' => 'Cookies  🍪',
                    'backgroundColor' => ['rgb(255, 99, 132, .4)','rgb(255, 88, 132)','rgba(45, 220, 126, .4)'],
                    'borderColor' => ['rgb(255, 99, 132)','rgb(255, 99, 132, .4)','rgba(45, 220, 126)'],
                    'data' => [60, 10, 30],
                    'tension' => 0.4,
                ],
            ],
        ]);
        $camembert->setOptions([
            'maintainAspectRatio' => false,
        ]);        
        return $this->render('stats/stats.html.twig', [
            'camembert' => $camembert,
            'chart' => $chart,
            'datas' => $datas,
            'machineNames' => $machinesnamearrays,
            'selectdatestrings' => $selectdateString,
        ]);
    }
}
