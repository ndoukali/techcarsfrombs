<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\TableJournaleRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TableJournaleRepository::class)]
#[ApiResource()]
class TableJournale
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $machineName = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $dateheure = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $pointnumber = null;

    #[ORM\Column(length: 10, nullable: true)]
    private ?string $steptime = null;
    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $UpdatedAt = null;

    #[ORM\Column(nullable: true)]
    private ?int $packetTime = null;

    #[ORM\Column(nullable: true)]
    private ?int $awakeSince = null;

    #[ORM\Column(length: 20, nullable: true)]
    private ?string $rowinarray = null;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
        $this->UpdatedAt = new \DateTimeImmutable();
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMachineName(): ?string
    {
        return $this->machineName;
    }

    public function setMachineName(?string $machineName): static
    {
        $this->machineName = $machineName;

        return $this;
    }

    public function getDateheure(): ?string
    {
        return $this->dateheure;
    }

    public function setDateheure(?string $dateheure): static
    {
        $this->dateheure = $dateheure;

        return $this;
    }

    public function getPointnumber(): ?string
    {
        return $this->pointnumber;
    }

    public function setPointnumber(?string $pointnumber): static
    {
        $this->pointnumber = $pointnumber;

        return $this;
    }

    public function getSteptime(): ?string
    {
        return $this->steptime;
    }

    public function setSteptime(?string $steptime): static
    {
        $this->steptime = $steptime;

        return $this;
    }
     public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->UpdatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $UpdatedAt): static
    {
        $this->UpdatedAt = $UpdatedAt;

        return $this;
    }

    public function getPacketTime(): ?int
    {
        return $this->packetTime;
    }

    public function setPacketTime(?int $packetTime): static
    {
        $this->packetTime = $packetTime;

        return $this;
    }

    public function getAwakeSince(): ?int
    {
        return $this->awakeSince;
    }

    public function setAwakeSince(?int $awakeSince): static
    {
        $this->awakeSince = $awakeSince;

        return $this;
    }

    public function getRowinarray(): ?string
    {
        return $this->rowinarray;
    }

    public function setRowinarray(?string $rowinarray): static
    {
        $this->rowinarray = $rowinarray;

        return $this;
    }
}
